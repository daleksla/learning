import sys 
# Takes first name and last name via command 
# line arguments and then display them 
print("Output from Python") #note: the code won't print here - it gets redirected to the stdout channel
print("First name: " + sys.argv[1]) 
print("Last name: " + sys.argv[2]) 

# save the script as hello.py 
